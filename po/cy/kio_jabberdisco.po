msgid ""
msgstr ""
"Project-Id-Version: kio_jabberdisco\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-05-20 03:10+0200\n"
"PO-Revision-Date: 2005-01-28 17:54+0000\n"
"Last-Translator: KDE <kde-i18n-doc@kde.org>\n"
"Language-Team: KDE <kde-i18n-doc@kde.org>\n"
"Language: cy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: jabberdisco.cpp:96 jabberdisco.cpp:183
#, kde-format
msgid "TLS"
msgstr "TLS"

#: jabberdisco.cpp:166
#, kde-format
msgid "The server certificate is invalid. Do you want to continue? "
msgstr ""

#: jabberdisco.cpp:167
#, kde-format
msgid "Certificate Warning"
msgstr ""

#: jabberdisco.cpp:284
#, kde-format
msgid "The login details are incorrect. Do you want to try again?"
msgstr ""
